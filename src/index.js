import React, { useRef } from 'react';
import ReactDOM from 'react-dom';

import Editor from "@monaco-editor/react";
import './index.css';

function App() {
  const editorRef = useRef(null);
  var codeState = true;

  function handleEditorDidMount(editor, monaco) {
    editorRef.current = editor;
  }

  function showValue() {

    if(!codeState) {
      alert("There are error(s) in your code, please review it.");
      return;
    }

    var codeEntity = {
      Id: 1,
      Text: editorRef.current.getValue(),
      Response: ""
    };

    fetch('/api/values', {
      method: 'post',
      headers: {
        'Content-Type':'application/json',
        'Accept':'*/*'
      },
      body: JSON.stringify(codeEntity)
    })
    .then(res => res.json())
    .then((data) => {
      var outputTextArea = document.getElementById("outputTextArea");
      outputTextArea.className = "";
      if(data.response === "Error"){
        outputTextArea.classList.add("error-message");
        outputTextArea.value = data.error;
      }else{
        outputTextArea.classList.add("output-message");
        outputTextArea.value = data.output;
      }
    })
    .catch(console.log)
  }

  function handleEditorValidation(markers) {
    console.clear();
    var thereIsErrors = markers.reduce(function(a, b){
      return a + (b.severity > 2 ? 1: 0); 
    }, 0);
    codeState =  thereIsErrors > 0 ? false : true;
    if(!codeState){
      console.log('%cThere are errors in your code:', 'background: #e63946; color: #1d3557; padding: 2px; border-radius:2px; font-size:18px; font-weight: bold;');
      markers.forEach(marker => {
        console.log(`%c Line: ${marker.startLineNumber}, Column: ${marker.startColumn}, Message: ${ marker.message } `, 'background: #e63946; color: #1d3557; padding: 2px; border-radius:2px; font-size:18px; font-weight: bold;');
      });
    } else {
      console.log('%cYour code looks good! (you can send it now)', 'background: #006d77; color: #edf6f9; padding: 2px; border-radius:2px; font-size:18px; font-weight: bold;');
    }
  }

  return (
    <>
      <button onClick={showValue}>Send Code</button>
      <br/>
      <Editor
        height="90vh"
        defaultLanguage="rust"
        defaultValue="// some comment"
        onMount={handleEditorDidMount}
        onValidate={handleEditorValidation}
      />
    </>
  );
}

function Output() {  
  return (
    <>
      <h1>Output:</h1>
      <br/>
      <textarea 
        id="outputTextArea" 
        placeholder="Your code output..."
      ></textarea>
    </>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);

const outputElement = document.getElementById("output");
ReactDOM.render(<Output />, outputElement);